//
//  ChatViewController.swift
//  SimpleChatApplication
//
//  Created by coozplz on 2016. 1. 5..
//  Copyright © 2016년 coozplz. All rights reserved.
//
import UIKit




class ChatViewController : UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, NetClientDelegate {
    @IBOutlet weak var messageInputTextView: UITextField!
    @IBOutlet weak var messageTableView: UITableView!
    @IBOutlet weak var userIdLabel: UILabel!
    var userId:String?
    private let sizingCell = ChatMessageCell()
    var messages : [ChatMessage] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userIdLabel.text = userId
        messageTableView.allowsSelection = false
        messageTableView.separatorStyle = .None
        messageTableView.registerClass(ChatMessageCell.classForCoder(), forCellReuseIdentifier: "messageCell")
        messageInputTextView.delegate = self
        
        NetClient.sharedInstance.delegate = self
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "receivedResponseNotification:", name: NotificationKey.SendMessageResponseNotification.rawValue, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "receivedMessageNotification:", name: NotificationKey.ReceiveMessageNotification.rawValue, object: nil)
    }
    
    
    override func delete(sender: AnyObject?) {
        print("ChatViewController, delete")
    }



    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func onSendMessageAction(sender: AnyObject) {
        let inputMessage:String = messageInputTextView.text!
        
        if inputMessage.characters.count == 0 {
            return
        }
        
        let chatMessage:ChatMessage = ChatMessage(message: inputMessage, direction: ChatMessage.Direction.SENT)
        self.addMessage(chatMessage)
        
        let data : Dictionary<String, AnyObject> = ["message" : inputMessage]
        let commandData : CommandData = CommandData(command: CommandCode.SEND_MESSAGE_REQUEST, data: data)
        NetClient.sharedInstance.sendToServer(commandData)

        messageInputTextView.text = ""
    }
    
    @IBAction func onLogoutAction(sender: AnyObject) {
        
        let confirmDialogController: UIAlertController = UIAlertController(title: "Confirm", message: "로그아웃하시겠습니까?", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        confirmDialogController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {(alertAction : UIAlertAction!) in

            let commandData : CommandData = CommandData(command: CommandCode.LOGOUT_REQUEST)
            NetClient.sharedInstance.sendToServer(commandData)
            
            self.performSegueWithIdentifier("segue_to_loginviewcontroller", sender: self)
        }))
        
        confirmDialogController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: {(alertAction: UIAlertAction!) in
            confirmDialogController.dismissViewControllerAnimated(true, completion: nil)
        }))
        self.presentViewController(confirmDialogController, animated: true, completion: nil)
    }

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messages.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("messageCell", forIndexPath: indexPath) as! ChatMessageCell
        let message = self.messages[indexPath.row]
        cell.setupWithMessage(message)
        return cell
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let padding: CGFloat = 10.0
        sizingCell.bounds.size.width = CGRectGetWidth(self.view.bounds)
        let height = self.sizingCell.setupWithMessage(messages[indexPath.row]).height + padding;
        return height
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField.text?.characters.count == 0 {
            return false
        }
        
        self.onSendMessageAction(textField)
        return true
    }
    
    
    
    private func scrollToBottom() {
        if messages.count > 0 {
            var lastItemIndex = self.messageTableView.numberOfRowsInSection(0) - 1
            
            if lastItemIndex < 0 {
                lastItemIndex = 0
            }
            
            let lastIndexPath = NSIndexPath(forRow: lastItemIndex, inSection: 0)
            self.messageTableView.scrollToRowAtIndexPath(lastIndexPath, atScrollPosition: .Bottom, animated: false)
        }
    }
    
    
    func addMessage(message : ChatMessage) {
        messages.append(message)
        
        self.messageTableView.reloadData()
        self.scrollToBottom()
        
    }
    
    
    func disconnected() {
        let confirmDialogController: UIAlertController = UIAlertController(title: "Warn", message: "소켓연결이 종료되었습니다.", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        confirmDialogController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {(alertAction : UIAlertAction!) in
            self.performSegueWithIdentifier("segue_to_loginviewcontroller", sender: self)
        }))
        self.presentViewController(confirmDialogController, animated: true, completion: nil)
    }
    
    @objc func receivedResponseNotification(notification: NSNotification) {
        
    }
    
    @objc func receivedMessageNotification(notification: NSNotification) {
        let message:String! = notification.userInfo!["message"] as! String
        let receivedChatMessage:ChatMessage = ChatMessage(message: message, direction: ChatMessage.Direction.RECEIVED)
        self.addMessage(receivedChatMessage)
    }
}