//
//  AlertHelper.swift
//  SimpleChatApplication
//
//  Created by coozplz on 2016. 1. 5..
//  Copyright © 2016년 coozplz. All rights reserved.
//

import Foundation
import UIKit

class AlertHelper  {
    private init () {

    }
    static let sharedInstance = AlertHelper()
    
    func showWarningAlert(sender: UIViewController, title: String, message : String, buttonText: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: buttonText, style: UIAlertActionStyle.Cancel, handler: nil))
        sender.presentViewController(alert, animated: true, completion: nil)
        
    }
}