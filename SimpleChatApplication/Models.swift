//
//  Models.swift
//  SimpleChatApplication
//
//  Created by coozplz on 2016. 1. 6..
//  Copyright © 2016년 coozplz. All rights reserved.
//

import Foundation



//
// 구조체를 NotificationCenter를 이용하여 전달하기 위한 래핑 객체
//
class Wrapper<T> {
    var wrapperValue : T?
    
    init (value : T) {
        self.wrapperValue = value
    }
}


//
// 접속 정보를 저장하는 객체
//
struct ConnectionInfo {
    var host : String!
    var port : Int!
    var userId : String!
    var password : String!
    
    init(host : String, port : Int, userId : String, password: String) {
        self.host = host
        self.port = port
        self.userId = userId
        self.password = password
    }
    
    var description : String  {
        return "host=\(host), port=\(port), userId=\(userId), password=\(password)"
    }
}


class NetworkHeader {
    var headerLenData : NSMutableData = NSMutableData()
    var bodyData : NSMutableData = NSMutableData()
    var bodySize : Int = 0
    
    func clear() {
        headerLenData.setData(NSData())
        bodyData.setData(NSData())
        bodySize = 0
    }
}


class CommandData : NSObject {
    var command : CommandCode
    var data : Dictionary<String, AnyObject>?
    
    init (command: CommandCode, data:Dictionary<String, AnyObject> = [:]) {
        self.command = command
        self.data = data
    }
    
    override var description : String {
        return "command=\(command), data=\(data)"
    }
    
    func toNSData() -> NSData {
        var header:Dictionary<String, AnyObject> = ["commandCode" : command.rawValue]
        
        
        if self.data != nil {
            header["data"] = self.data
        }
        
        
        var serializedData : NSData?
        
        do {
            serializedData = try NSJSONSerialization.dataWithJSONObject(header, options: .PrettyPrinted)
        }
        catch {
            print("JSON parse error, \(error)")
        }
        
        return serializedData!
    }
}


class ResponseCommandData : CommandData {
    var success : Int!
    var reason : String!
    
    override init(command:CommandCode, data: Dictionary<String, AnyObject>) {
        super.init(command: command, data: data)
        success = data["success"] as? Int
        reason = data["reason"] as? String
    }
}


class CommandParser {
    static let sharedInstance = CommandParser()

    private init() {}
    
    func parseHeader(receivedData:NSData) -> CommandData {
        var command : CommandCode?
        var data : Dictionary<String, AnyObject>?
        do {
            let receivedDict:NSDictionary = try NSJSONSerialization.JSONObjectWithData(receivedData, options: .MutableContainers) as! NSDictionary
            let commandString:String = (receivedDict["commandCode"] as? String)!
            
            command = CommandCode(rawValue: commandString)!
            data = receivedDict["data"] as? Dictionary<String, AnyObject>
        }
        catch {
            print("Parsing error \(error)")
        }
        
        return CommandData(command: command!, data: data!)
    }
}