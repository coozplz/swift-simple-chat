//
//  CommandCode.swift
//  SimpleChatApplication
//
//  Created by coozplz on 2016. 1. 6..
//  Copyright © 2016년 coozplz. All rights reserved.
//

import Foundation


enum CommandCode : String {
    case LOGIN_REQUEST          = "LOGIN_REQUEST"
    case INVALID_REQUEST        = "INVALID_REQUEST"
    case LOGIN_RESPONSE         = "LOGIN_RESPONSE"
    case LOGOUT_REQUEST         = "LOGOUT_REQUEST"
    case SEND_MESSAGE_REQUEST   = "SEND_MESSAGE_REQUEST"
    case SEND_MESSAGE_RESPONSE  = "SEND_MESSAGE_RESPONSE"
    case NOTIFY_MESSAGE         = "NOTIFY_MESSAGE"
    case NOTIFY_USER_LOGIN      = "NOTIFY_USER_LOGIN"
    case NOTIFY_USER_LOGOUT     = "NOTIFY_USER_LOGOUT"
    case NOTIFY_DUPLICATED_LOGIN = "NOTIFY_DUPLICATED_LOGIN"
    
}