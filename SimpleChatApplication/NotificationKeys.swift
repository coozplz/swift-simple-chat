//
//  NotificationKeys.swift
//  SimpleChatApplication
//
//  Created by coozplz on 2016. 1. 5..
//  Copyright © 2016년 coozplz. All rights reserved.
//

import Foundation


enum NotificationKey : String {
    case MakeConnection = "MakeConnection"
    case MakeConnectionCallback = "MakeConnectionCallback"
    case SendMessageResponseNotification = "SendMessageResponseNotification"
    case ReceiveMessageNotification = "ReceiveMessageNotification"
}