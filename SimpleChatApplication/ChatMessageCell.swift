//
//  ChatMessageCell.swift
//  SimpleChatApplication
//
//  Created by coozplz on 2016. 1. 7..
//  Copyright © 2016년 coozplz. All rights reserved.
//

import Foundation
import UIKit


class ChatMessage : NSObject {
    enum Direction : Int {
        case SENT = 1
        case RECEIVED = 2
    }
    
    
    var direction : Direction
    var message : String
    var time : NSTimeInterval?
    
    
    required init ( message : String, direction: Direction, time : NSTimeInterval? = nil) {
        self.message = message
        self.direction = direction
        self.time = time
        
    }
}
class ChatMessageCell : UITableViewCell {
    
    private let padding : CGFloat = 5.0
    private let minimumHeight : CGFloat = 30.0
    
    private var size = CGSizeZero
    
    private var maxSize : CGSize {
        get {
            let maxWidth = CGRectGetWidth(self.bounds)
            let maxHeight = CGFloat.max
            return CGSize(width: maxWidth, height: maxHeight)
        }
    }
    
    private struct Appearance {
        static let receivedMessageColor : UIColor  = UIColor(red: 220 / 255.0, green: 220 / 255.0, blue: 220 / 255.0, alpha: 1.0)
        static let receivedMessageFontColor : UIColor = UIColor.blackColor()
        static let sentMessageColor : UIColor = UIColor(red: 50 / 255.0, green: 205 / 255.0, blue: 51 / 255.0, alpha: 1.0)
        static let sentMessageFontColor : UIColor = UIColor.whiteColor()
        static let font: UIFont = UIFont.systemFontOfSize(17.0)
    }
    
    private lazy var textView: MessageTextView = {
        let textView = MessageTextView(frame: CGRectZero, textContainer: nil)
        self.contentView.addSubview(textView)
        return textView
    }()
    
    
    private class MessageTextView : UITextView {
        override init(frame: CGRect, textContainer: NSTextContainer?) {
            super.init(frame: frame, textContainer: textContainer)
            
            self.font = Appearance.font
            self.scrollEnabled = false
            self.editable = false
            self.textContainerInset = UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7)
            self.layer.cornerRadius = 15
            self.layer.borderWidth = 2.0
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("Not implemented")
        }
    }
    
    func setupWithMessage(message: ChatMessage) -> CGSize {
//        let formatter: NSDateFormatter = NSDateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd-HH-mm-ss"
//        let dateTimePrefix: String = formatter.stringFromDate(NSDate(timeIntervalSince1970: message.time!))
        
        textView.text = message.message
        size = textView.sizeThatFits(maxSize)
        
        if size.height < minimumHeight {
            size.height = minimumHeight
        }
        
        textView.bounds.size = size
        self.styleTextViewForDirection(message.direction)
        return size
    }
    
    
    private func styleTextViewForDirection(direction : ChatMessage.Direction) {
        let halfTextViewWidth = CGRectGetWidth(self.textView.bounds) / 2.0
        let targetX = halfTextViewWidth + padding
        let halfTextViewHeight = CGRectGetHeight(self.textView.bounds) / 2.0
        
        
        switch (direction) {
        case .RECEIVED:
            self.textView.center.x = targetX
            self.textView.center.y = halfTextViewHeight
            self.textView.layer.borderColor = Appearance.receivedMessageColor.CGColor
            self.textView.layer.backgroundColor = Appearance.receivedMessageColor.CGColor
            self.textView.textColor = Appearance.receivedMessageFontColor
            break
            
        case .SENT :
            self.textView.center.x = CGRectGetWidth(self.bounds) - targetX
            self.textView.center.y = halfTextViewHeight
            self.textView.layer.borderColor = Appearance.sentMessageColor.CGColor
            self.textView.layer.backgroundColor = Appearance.sentMessageColor.CGColor
            self.textView.textColor = Appearance.sentMessageFontColor
            break
        }
        
    }
}