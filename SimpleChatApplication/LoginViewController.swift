//
//  ViewController.swift
//  SimpleChatApplication
//
//  Created by coozplz on 2016. 1. 5..
//  Copyright © 2016년 coozplz. All rights reserved.
//

import UIKit

let MAX_PORT_LEN : Int = 5
let MAX_HOST_LEN : Int = 17

//
// 텍스트 필드를 구분하기 위한 태그값 
//
enum TextFieldTag : Int {
    case TF_HOST = 0
    case TF_PORT = 1
    case TF_USERID = 2
}


//
// 로그인 뷰 화면을 처리한다.
//
class LoginViewController : UIViewController, UITextFieldDelegate {
    
    let IP_VALIDATION_REGEX : String = "^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$"
    let PORT_VALIDATION_REGEX : String = "^[0-9]{4}|[0-9]{5}$"
    
    @IBOutlet weak var tfHost : UITextField!
    @IBOutlet weak var tfPort : UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfUserId : UITextField!
    
    
    override func viewDidLoad() {

        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "connectedCallback:", name: NotificationKey.MakeConnectionCallback.rawValue, object: nil)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NetClient.sharedInstance.closeConnection()
    }
    
    
    override func viewDidDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NotificationKey.MakeConnectionCallback.rawValue, object: nil);
    }
    
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    
        switch textField.tag {
        case TextFieldTag.TF_HOST.rawValue:

            // 호스트 주소는 IPv4 형식으로 최대 17자 까지만 입력을 받도록 제한한다.
            guard let text = textField.text else {
                return true
            }
            
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= MAX_HOST_LEN
            
        case TextFieldTag.TF_PORT.rawValue:
            // 포트는 최대 5자리까지 입력받도록 제한한다.
            guard let text = textField.text else {
                return true
            }
            
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= MAX_PORT_LEN
            
        case TextFieldTag.TF_USERID.rawValue:
            break
        default :
            break
        }
        
        
        return true
    }
    
    @IBAction func loginButtonClicked(sender: UIButton) {
      
        let host : String? = self.tfHost.text
        let port : String? = self.tfPort.text
        let userId : String? = self.tfUserId.text
        let password: String? = self.tfPassword.text
        
        if host!.isEmpty || port!.isEmpty || userId!.isEmpty || password!.isEmpty {
            AlertHelper.sharedInstance.showWarningAlert(self, title: "Warning", message: "입력값이 올바르지 않습니다.", buttonText: "OK");
            return
        }
        
        do {
            let portValidationRegex : NSRegularExpression = try NSRegularExpression(pattern: PORT_VALIDATION_REGEX, options: .CaseInsensitive)
            
            let length:Int! = port!.characters.count
            let range:NSRange = NSMakeRange(0, length)

            let ipaddressValidationRegex : NSRegularExpression = try NSRegularExpression(pattern: IP_VALIDATION_REGEX, options: .CaseInsensitive)
            
            
            if ipaddressValidationRegex.matchesInString(host!, options: .Anchored, range: NSMakeRange(0, host!.characters.count)).count == 0 {
                AlertHelper.sharedInstance.showWarningAlert(self, title: "Warning", message: "IP 정보가 올바르지 않습니다.", buttonText: "OK");
                return
            }

            
            if portValidationRegex.matchesInString(port!, options: NSMatchingOptions.Anchored, range: range).count == 0 {
                AlertHelper.sharedInstance.showWarningAlert(self, title: "Warning", message: "포트 정보가 올바르지 않습니다.", buttonText: "OK");
                return
            }
            
        } catch {
            print("Regex error \(error)")
        }

        
        let connectionInfo : ConnectionInfo = ConnectionInfo(host: host!, port: Int(port!)!, userId: userId!, password: password!)
        
        
        let connectionResult:Bool = NetClient.sharedInstance.connectToServer(connectionInfo)
        
        if !connectionResult {
            AlertHelper.sharedInstance.showWarningAlert(self, title: "Warning", message: "접속에 실패하였습니다.", buttonText: "OK")
        }
    }
    
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segue_login_to_chat" {
            let destinationViewController:ChatViewController = (segue.destinationViewController as? ChatViewController)!
            destinationViewController.userId = tfUserId.text!
        }
    }
    
    
    func connectedCallback(notification:NSNotification) {
        
        if let info = notification.userInfo as? Dictionary<String, AnyObject> {
            let responseData: ResponseCommandData = (info["data"] as? ResponseCommandData)!
            
            if (responseData.success == 1) {
                self.performSegueWithIdentifier("segue_login_to_chat", sender: self)
                return
            }
        }
        
        AlertHelper.sharedInstance.showWarningAlert(self, title: "Warning", message: "로그인에 실패하였습니다.", buttonText: "확인")
    }
}

