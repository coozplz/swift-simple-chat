//
//  NetCore.swift
//  SimpleChatApplication
//
//  Created by coozplz on 2016. 1. 5..
//  Copyright © 2016년 coozplz. All rights reserved.
//

import Foundation
import UIKit
import CoreFoundation



protocol NetClientDelegate : class {
    func disconnected()
}

//
// 네트워크 송수신 처리를 위한 객체
//
class NetClient : NSObject, NSStreamDelegate {
    static let sharedInstance = NetClient()
    
    weak var delegate : NetClientDelegate?
    
    private var host : String?
    private var port : Int?
    private var inputStream : NSInputStream?
    private var outputStream : NSOutputStream?
    
    private var header : NetworkHeader?
    private var dataWriteQueue : Array<NSData>?
    
    

    
    private override init() {
        super.init()
        header = NetworkHeader()
        dataWriteQueue = Array<NSData>()

    }
    
    
    
    
    func connectToServer(connectionInfo: ConnectionInfo) -> Bool {
        var inp:NSInputStream?
        var outp:NSOutputStream?
        
        NSStream.getStreamsToHostWithName(connectionInfo.host, port: connectionInfo.port, inputStream: &inp , outputStream: &outp)
        
        inputStream = inp!
        outputStream = outp!
        
        inputStream?.delegate = self
        outputStream?.delegate = self
        
        
        inputStream?.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        outputStream?.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        
        inputStream!.open()
        outputStream!.open()
        
        if inputStream?.streamStatus == NSStreamStatus.Error {
            closeConnection()
            return false
        }
        
        
        let userData : Dictionary! = [
            "userId" : connectionInfo.userId,
            "password" : connectionInfo.password
        ]
        
        let commandData : CommandData = CommandData(command: CommandCode.LOGIN_REQUEST, data: userData)
        
        print(inputStream!.streamStatus.rawValue)
        print(inputStream!.streamError)
        
        if inputStream!.streamError != nil {
            closeConnection()
            return false
        }
        sendToServer(commandData)
        print("네트워크 접속 시도")
        return true
    
    }
    
    
    
    
    private func didReceivedDataFromStream(receivedBytes:NSData) {
        var remainCount:Int! = receivedBytes.length
        let totalCount:Int! = remainCount
        
        if header?.headerLenData.length == 0 {
            var bufSize:Int = sizeof(Int) - (header?.headerLenData.length)!
            bufSize = min(bufSize, remainCount)
            
            let range : NSRange = NSMakeRange(totalCount-remainCount, bufSize)
            
            let headerLenData:NSData = receivedBytes.subdataWithRange(range)
            remainCount = remainCount - bufSize
            
            header?.headerLenData.appendData(headerLenData)
            print(header?.headerLenData)
        }
        
        if header?.headerLenData.length == sizeof(Int) {
            var bodySize:UInt32 = 0
            header?.headerLenData.getBytes(&bodySize, length: sizeofValue(bodySize))
            print("\(header?.headerLenData), bodySize=\(bodySize)")
            bodySize = CFSwapInt32BigToHost(bodySize)
            header?.bodySize = Int(bodySize)
            
            
            if header?.bodySize > header?.bodyData.length {
                var bufSize : Int = header!.bodySize - (header?.bodyData.length)!
                bufSize = min(bufSize, remainCount)
                let readableBodyRange:NSRange = NSMakeRange(totalCount-remainCount, bufSize)
                let bodyData : NSData = receivedBytes.subdataWithRange(readableBodyRange)
                header?.bodyData.appendData(bodyData)
                remainCount = remainCount - bufSize
            }
        }
        
        if header?.headerLenData.length == sizeof(Int) && header?.bodyData.length == header?.bodySize {
            processReceivedData()
        }
        
        
    }
    
    
    private func toByteArray<T>(var value: T) -> [UInt8] {
        return withUnsafePointer(&value) {
            Array(UnsafeBufferPointer(start: UnsafePointer<UInt8>($0), count: sizeof(T)))
        }
    }
    
    
    private func convertUnsafePointerToArray(length: Int, data: UnsafePointer<UInt8>) -> [UInt8] {
        let buffer = UnsafeBufferPointer(start: data, count: length);
        return Array(buffer)
    }
    
    private func processReceivedData() {
        
        let commandHeader:CommandData = CommandParser.sharedInstance.parseHeader((header?.bodyData)!)
        
        print("RECV = \(commandHeader)")
            
        switch(commandHeader.command) {
            
        case .LOGIN_RESPONSE:
            
            let responseHeader:ResponseCommandData = ResponseCommandData(command:commandHeader.command, data: commandHeader.data!)
            var dataDict = Dictionary<String, AnyObject>()
            dataDict["data"] = responseHeader
            
            NSNotificationCenter.defaultCenter().postNotificationName(NotificationKey.MakeConnectionCallback.rawValue, object: nil, userInfo: dataDict)
            
            break
            
        case .SEND_MESSAGE_RESPONSE:
            NSNotificationCenter.defaultCenter().postNotificationName(NotificationKey.SendMessageResponseNotification.rawValue, object: nil, userInfo: commandHeader.data)
            
            break
            
        case .NOTIFY_MESSAGE:
            NSNotificationCenter.defaultCenter().postNotificationName(NotificationKey.ReceiveMessageNotification.rawValue, object: nil, userInfo: commandHeader.data)
            
            break
        default:
            break
        }
        
        
        
        header?.clear()
    }
    
    
    /**
    * 데이터를 서버로 송신한다.
    */
    private func sendToServer() {
        
        if dataWriteQueue?.count == 0 {
            return
        }
        let data:NSData = (dataWriteQueue?.popLast())!
        let dataPacketTemp  = UnsafePointer<UInt8>(data.bytes)


        
        var dataLenPacket : [UInt8]?
        let dataPacket : [UInt8]! = convertUnsafePointerToArray(data.length, data:dataPacketTemp)
        if CFByteOrderGetCurrent().littleEndian == Int(CFByteOrderLittleEndian.rawValue) {
            // 서버가 BIG ENDIAN인 경우 변환한다.
            dataLenPacket = toByteArray(CFSwapInt32(UInt32(data.length)))
            
        } else {
            // 서버가 LITTLE ENDIAN 이라면 그대로 사용한다.
            dataLenPacket = toByteArray(data.length)
        }
        
        outputStream?.write(dataLenPacket!, maxLength: sizeof(Int))
        outputStream?.write(dataPacket, maxLength: data.length)
        print("sent data!!")
    }
//    
    
    func sendToServer(commandData:CommandData) {
        dataWriteQueue?.append(commandData.toNSData())
        sendToServer()
    }
    
    
    func closeConnection() {
        dataWriteQueue?.removeAll()
        inputStream?.close()
        inputStream?.removeFromRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        outputStream?.close()
        outputStream?.removeFromRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        
        if self.delegate != nil {
            self.delegate?.disconnected()
        }
    }
    
    
    
    func stream(aStream: NSStream, handleEvent eventCode: NSStreamEvent) {
        switch ( eventCode ) {

        case NSStreamEvent.ErrorOccurred:
            print("ErrorOccurred!!!")
            closeConnection()
            NSNotificationCenter.defaultCenter().postNotificationName(NotificationKey.MakeConnectionCallback.rawValue, object: nil, userInfo: nil)
            break
        
            
        case NSStreamEvent.EndEncountered:
            print("EndEncountered!!")
            closeConnection()
            break
        
            
        case NSStreamEvent.None:
            break
        
            
        case NSStreamEvent.HasBytesAvailable:
        
            if aStream == inputStream {
                
                if inputStream?.hasBytesAvailable == false {
                    print("소켓에 수신할 데이터가 없습니다.")
                    return
                }
                
                var buffer = [UInt8](count: 4096, repeatedValue:0)
                let data : NSMutableData = NSMutableData()
                let readSize:Int = inputStream!.read(&buffer, maxLength: buffer.count)
                
                if readSize == 0 {
                    print("소켓에 수신할 데이터가 없습니다.")
                    return
                }
                
                if readSize < 0 {
                    print("소켓 연결이 종료되었습니다.")
                    
                    closeConnection()
                    return
                }
                
                data.appendBytes(buffer, length: readSize)
                didReceivedDataFromStream(data)
                
            }
            break
        
            
        case NSStreamEvent.OpenCompleted:
            print("소켓이 연결 되었습니다.")
            sendToServer()
            break
        
            
        case NSStreamEvent.HasSpaceAvailable:
            print("HasSpaceAvailable")
            
            break
            
        default : break
        
        }
        
    }

}

